package com.atendimentoautomacao.hooks;

import com.atendimentoautomacao.GlobalParameters;
import com.atendimentoautomacao.utils.DriverUtils;
import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.BeforeSuite;

public class Hooks {
    @BeforeSuite
    public void beforeSuite(){
        new GlobalParameters();
    }

    @BeforeScenario
    public void beforeScenario() {
        DriverUtils.createInstance();
        DriverUtils.INSTANCE.manage().window().maximize();
        DriverUtils.INSTANCE.navigate().to(GlobalParameters.URL_DEFAULT);
    }

    @AfterScenario
    public void afterScenario(){
        DriverUtils.quitInstace();
    }
}
