package com.atendimentoautomacao.steps;

import com.atendimentoautomacao.pages.NovoAtendimentoPage;

import com.thoughtworks.gauge.Step;

import static org.assertj.core.api.Assertions.assertThat;

public class ProrrogarGuiaAtendimentoDomiciliar {
    NovoAtendimentoPage novoAtendimentoPage = new NovoAtendimentoPage();

    @Step("Clicar em Prorrogar Guia")
    public void clicarEmProrrogarGuiaAtendimentoDomiciliar() {
        novoAtendimentoPage.clicarEmProrrogarGuiaAtendimento();
    }

    @Step("Informo a quantidade de diárias adicionais solicitadas <1>")
    public void preencherQtdeDiariasAdicionaisSolicitadas(String qtdeDiariasAdicionais) {
        novoAtendimentoPage.preencherQtdeDiariasAdicionaisSolicitadas(qtdeDiariasAdicionais);
    }

    @Step("Informo o tipo de acomodação <Quarto privativo / Particular>")
    public void preencherTipoAcomodacao(String tipoAcomodacao) {
        novoAtendimentoPage.preencherTipoAcomodacaoSolicitada(tipoAcomodacao);
    }

    @Step("Clicar em Finalizar e Gerar a Guia Prorrogação")
    public void clicarEmFinalizarEGerarGuiaProrrogacao() {
        novoAtendimentoPage.clicarmEmFinalizarEGerarGuiaProrrogacao();
    }

    @Step("Verificar se Prorrogação foi realizada com sucesso")
    public void verificarProrrogacaoGuiaAtendimentoDomiciliar() {
        assertThat(novoAtendimentoPage.retornaResultadoProrrogacaoGuiaDomiciliar()).isEqualTo("Prorrogação de Internação");
        assertThat(novoAtendimentoPage.retornaResultadoGuiaEmAnalise()).isEqualTo("Em análise");
    }

}
