package com.atendimentoautomacao.steps;

import com.atendimentoautomacao.pages.NovoAtendimentoPage;
import com.thoughtworks.gauge.Step;

import static org.assertj.core.api.Assertions.assertThat;

public class GerarGuiaInternacao {

    NovoAtendimentoPage novoAtendimentoPage = new NovoAtendimentoPage();

    @Step("Verificar se guia de Internação foi gerada com sucesso")
    public void verificarGuiaInternacaoGeradaComSucesso() {
        assertThat(novoAtendimentoPage.retornaResultadoGuiaEmAnalise()).isEqualTo("Em análise");
        assertThat(novoAtendimentoPage.retornaConfirmacaoNumCarteira()).isEqualTo("02965382569000");
    }
}
