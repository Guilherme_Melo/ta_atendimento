package com.atendimentoautomacao.steps;

import com.atendimentoautomacao.pages.NovoAtendimentoPage;
import com.thoughtworks.gauge.Step;

import static org.assertj.core.api.Assertions.assertThat;

public class CapturarGuiaUrgenciaEEmergencia {
    NovoAtendimentoPage novoAtendimentoPage = new NovoAtendimentoPage();

    @Step("Verificar se guia de Urgência e Emergência foi capturada com sucesso")
    public void verificaCapturaGuiaUrgênciaEEmergencia() {
        assertThat(novoAtendimentoPage.retornaCapturaGuiaUrgenciaEEmergencia()).isEqualTo("Guia de Consulta de urgência e emergência");
        assertThat(novoAtendimentoPage.retornaResultadoCapturaGuia()).isEqualTo("Guia Capturada");
    }
}
