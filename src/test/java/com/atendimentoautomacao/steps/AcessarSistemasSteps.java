package com.atendimentoautomacao.steps;

import com.atendimentoautomacao.pages.AtendimentoMenuPrincipalPage;
import com.atendimentoautomacao.pages.SistemasPage;
import com.thoughtworks.gauge.Step;

import static org.assertj.core.api.Assertions.assertThat;

public class AcessarSistemasSteps {
    SistemasPage sistemasPage = new SistemasPage();
    AtendimentoMenuPrincipalPage atendimentoMenuPrincipalPage = new AtendimentoMenuPrincipalPage();

    @Step("Clicar no sistema Atendimento")
    public void clicarEmAtendimento() {
        sistemasPage.clicarEmAtendimento();
    }

    @Step("Verificar se sistema foi aberto")
    public void verificarSeSistemaFoiAberto() {
        assertThat(atendimentoMenuPrincipalPage.retornaTituloMenuPrincipal()).isEqualTo("Atendimento");
    }
}
