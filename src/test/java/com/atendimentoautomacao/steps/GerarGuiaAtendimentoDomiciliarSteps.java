package com.atendimentoautomacao.steps;



import com.atendimentoautomacao.pages.NovoAtendimentoPage;
import com.atendimentoautomacao.pages.SistemasPage;
import com.thoughtworks.gauge.Step;

import static org.assertj.core.api.Assertions.assertThat;

public class GerarGuiaAtendimentoDomiciliarSteps {
    SistemasPage sistemasPage = new SistemasPage();
    NovoAtendimentoPage novoAtendimentoPage = new NovoAtendimentoPage();

    @Step("Clicar em Novo Atendimento")
    public void clicarEmNovoAtendimento() {
        sistemasPage.clicarEmAtendimento();
    }

    @Step("Clicar em Solicitação de Autorização")
    public void clicarEmSolicitacaoDeAutorizacao() {
        novoAtendimentoPage.clicarEmSolicitacaoAutorizacao();
    }

    @Step("Informo o tipo de atendimento <atendimento>")
    public void informarTipoAtendimento(String tipoAtendimento) {
        novoAtendimentoPage.selecionarTipoAtendimento(tipoAtendimento);
    }

    @Step("Informo o número da carteira <carteira>")
    public void preencherNumeroCarteira(String numeroCarteira) {
        novoAtendimentoPage.preencherNumeroCarteira(numeroCarteira);
    }

    @Step("Clicar em Próximo passo")
    public void clicarProximoPasso() {
        novoAtendimentoPage.clicarEmProximoPasso();
    }

    @Step("Clicar em Fechar")
    public void clicarEmFechar() {
        novoAtendimentoPage.clicarEmFecharModalAlerta();
    }

    @Step("Informo o nome do Hospital <hospital>")
    public void informarNomeHospital(String idHospital) {
        novoAtendimentoPage.preencherIdHospital(idHospital);
    }

    @Step("Informo o nome do Profissional <profissional>")
    public void informarProfissional(String profissional) {
        novoAtendimentoPage.prencherNomeProfissionalSolicitante(profissional);
    }

    @Step("Informo a Data Sugerida <data>")
    public void informarDataSugerida(String dataSugerida) {
        novoAtendimentoPage.preencherDataSugerida(dataSugerida);
    }

    @Step("Informo o contratado solicitante <contratadoSolicitante>")
    public void informarContratadoSolicitante(String contratadoSolicitante) {
        novoAtendimentoPage.preencherContratadoSolicitante(contratadoSolicitante);
    }

    @Step("Informo o tipo de Indicação de Acidente <indicacaoAcidente>")
    public void informarIndicacaoAcidente(String indicacaoAcidente) {
        novoAtendimentoPage.preencherIndicacaoAcidente(indicacaoAcidente);
    }

    @Step("Informo o tipo de Carater do atendimento <carater>")
    public void informarCaraterAtendimento(String caraterAtendimento) {
        novoAtendimentoPage.preencherCaraterAtendimento(caraterAtendimento);
    }

    @Step("Informo o tipo de Internação <internacao>")
    public void informarTipoInternacao(String tipoInternacao) {
        novoAtendimentoPage.preencherTipoInternacao(tipoInternacao);
    }

    @Step("Informo o tipo de Regime de Internacao <regimeInternacao>")
    public void informarTipoRegimeInternacao(String regimeInternacao) {
        novoAtendimentoPage.preencherRegimeInternacao(regimeInternacao);
    }

    @Step("Informo a quantidade de diárias Solicitadas <diariasSolicitadas>")
    public void informarQuatidadeDiariasSolicitadas(String diariasSolicitadas) {
        novoAtendimentoPage.preencherQtdeDiariasSolicitadas(diariasSolicitadas);
    }

    @Step("Informo a indicação clínica <indicacaoClinica>")
    public void informarIndicacaoClinica(String indicacaoClinica) {
        novoAtendimentoPage.preencherIndicacaoClinica(indicacaoClinica);
    }

    @Step("Informo o CID Principal <cid>")
    public void informarCidPrincipal(String cidPrincipal) {
        novoAtendimentoPage.prencherCidPrincipal(cidPrincipal);
    }

    @Step("Anexo um documento do tipo PDF")
    public void anexarArquivoPDF() {
        novoAtendimentoPage.clicarEmAnexarDocumentos("src\\test\\resources\\files\\Teste.pdf");
    }

    @Step("Informo a Tabela de Procedimentos <tabela>")
    public void informarTabelaProcedimentos(String tabelaProcedimentos) {
        novoAtendimentoPage.preencherTipoTabelaProcedimentosComboBox(tabelaProcedimentos);
    }

    @Step("Informo a Descrição do Procedimento <descricaoProcedimento>")
    public void informarDescricaoProcedimento(String descricaoProcedimentoItem) {
        novoAtendimentoPage.preencherTipoProcedimentoTabela(descricaoProcedimentoItem);
    }

    @Step("Informo a Quantidade Solicitada <qtdeSolicitada>")
    public void informarQuantidadeSolicitada(String qtdeSolicitada) {
        novoAtendimentoPage.preencherQtdeSolicitadaTabela(qtdeSolicitada);
    }

    @Step("Clicar em Solicitar Autorização")
    public void clicarEmSolicitarAutorizacao() {
        novoAtendimentoPage.clicarEmSolicitarAutorizacaoLink();
    }

    @Step("Informo o número de Telefone <telefone>")
    public void preencherTelefoneGuia(String telefoneGuia) {
        novoAtendimentoPage.preencherTelefoneGuiaDomiciliar(telefoneGuia);
    }

    @Step("Informo o email <email>")
    public void preencherEmailGuia(String emailGuia) {
        novoAtendimentoPage.preencherEmailGuiaDomiciliar(emailGuia);
    }

    @Step("Clicar em Finalizar e Gerar a Guia")
    public void clicarEmFinalizarGerarGuia() {
        novoAtendimentoPage.clicarEmFinalizarEGerarGuia();
    }

    @Step("Verificar se guia foi gerada com sucesso")
    public void verificarSeGuiaFoiGerada() {
        assertThat(novoAtendimentoPage.retornaResultadoGuiaEmAnalise()).isEqualTo("Em análise");
    }

}

