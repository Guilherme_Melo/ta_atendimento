package com.atendimentoautomacao.steps;

import com.atendimentoautomacao.GlobalParameters;
import com.atendimentoautomacao.dbsteps.UsuariosDBSteps;
import com.atendimentoautomacao.pages.LoginPage;
import com.atendimentoautomacao.pages.MenuSuperiorPage;
import com.thoughtworks.gauge.Step;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginSteps {
    LoginPage loginPage = new LoginPage();
    MenuSuperiorPage menuSuperiorPage = new MenuSuperiorPage();

    @Step("Informo usuario <usuario>")
    public void prencherUsuario(String usuario) {
        loginPage.preencherUsuario(usuario);
    }

    @Step("Informo senha <senha>")
    public void prencherSenha(String senha) {
        loginPage.preencherSenha(senha);
    }

    @Step("Clico em entrar")
    public void clicarEmEntrar() {
        loginPage.clicarEmEntrar();
    }

    @Step("Usuario <usuario> é autenticado com sucesso")
    public void validaUsuarioAutenticado(String usuario){
        assertThat(menuSuperiorPage.retornaNomeMenuUsuario()).isEqualTo(usuario);
    }

    @Step("Efetuar login no sistema com usuário default")
    public void efetuarLoginNoSistemaComUsuarioDefault(){
        loginPage.preencherUsuario(GlobalParameters.USUARIO_DEFAULT);
        loginPage.preencherSenha(GlobalParameters.SENHA_DEFAULT);
        loginPage.clicarEmEntrar();
    }
}
