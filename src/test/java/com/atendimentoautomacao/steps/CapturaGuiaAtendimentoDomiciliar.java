package com.atendimentoautomacao.steps;

import com.atendimentoautomacao.GlobalParameters;
import com.atendimentoautomacao.pages.LoginPage;
import com.atendimentoautomacao.pages.NovoAtendimentoPage;
import com.atendimentoautomacao.pages.SistemasPage;
import com.thoughtworks.gauge.Step;

import static org.assertj.core.api.Assertions.*;

public class CapturaGuiaAtendimentoDomiciliar {
    LoginPage loginPage = new LoginPage();
    SistemasPage sistemasPage = new SistemasPage();
    NovoAtendimentoPage novoAtendimentoPage = new NovoAtendimentoPage();


    @Step("Clicar em Captura / Execução")
    public void clicarEmaCapturaExecucacao() {
        novoAtendimentoPage.clicarEmCapturarExecucao();
    }

    @Step("Clicar em Justificar a Não validação")
    public void clicarEmJustificarNaoValidacaoBiometria() {
        novoAtendimentoPage.clicarEmJustificarNaoValidacaoBiometria();
    }

    @Step("Preencher o motivo da não justificativa <motivo>")
    public void preencherMotivoNaoJustificativaBiometria(String motivoNaoJustificativa) {
        novoAtendimentoPage.preencherTipoMotivoJustificativaBiometria(motivoNaoJustificativa);
    }

    @Step("Preencher o texto da Justificativa <justificativa>")
    public void preencherTextoJustificativaBiometria(String textoJustificativa) {
        novoAtendimentoPage.preencherJustificativaNãoValidacaoBiometria(textoJustificativa);
    }

    @Step("Clicar em Salvar justificativa")
    public void clicarEmSalvarJustificativa() {
        novoAtendimentoPage.clicarEmSalvarMotivoJustificativaBiometria();
    }

    @Step("Clicar em Próximo passo Biometria")
    public void clicarEmProximoPasso() {
        novoAtendimentoPage.clicarEmProximoPassoBiometria();
    }

    @Step("Clicar em Fechar Biometria")
    public void clicarEmFechar() {
        novoAtendimentoPage.clicarEmFecharModalAlertaBiometria();
    }

    @Step("Clicar em Capturar Guia")
    public void clicarEmCapturarGuia() {
        novoAtendimentoPage.clicarEmCapturarGuia();
    }

    @Step("Verificar guia capturada com sucesso")
    public void verificarGuiaCapturada() {
        assertThat(novoAtendimentoPage.retornaResultadoCapturaGuia()).isEqualTo("Guia Capturada");
    }

    @Step("Anexo um documento para Prorrogação do tipo PDF")
    public void anexarArquivoPDF() {
        novoAtendimentoPage.clicarEmAnexarDocumentosProrrogacao("src\\test\\resources\\files\\Teste.pdf");
    }

    @Step("Acessar guia de Atendimento Domiciliar Capturada")
    public void capturarGuiaAtendimentoCompleta() {
        sistemasPage.clicarEmAtendimento();
        sistemasPage.clicarEmAtendimento();
        novoAtendimentoPage.clicarEmCapturarExecucao();
        novoAtendimentoPage.selecionarTipoAtendimento(GlobalParameters.TIPO_ATENDIMENTO_DEFAULT);
        novoAtendimentoPage.preencherNumeroCarteira(GlobalParameters.NUM_CARTEIRA_DEFAULT);
        novoAtendimentoPage.clicarEmJustificarNaoValidacaoBiometria();
        novoAtendimentoPage.preencherTipoMotivoJustificativaBiometria(GlobalParameters.MOTIVO_JUSTIFICATIVA_VAL_BIOMETRIA);
        novoAtendimentoPage.preencherJustificativaNãoValidacaoBiometria(GlobalParameters.TEXTO_JUSTIFICATIVA_VAL_BIOMETRIA);
        novoAtendimentoPage.clicarEmSalvarMotivoJustificativaBiometria();
        novoAtendimentoPage.clicarEmProximoPassoBiometria();
        novoAtendimentoPage.clicarEmFecharModalAlertaBiometria();
        novoAtendimentoPage.clicarEmCapturarGuia();
    }
}