package com.atendimentoautomacao.steps;

import com.atendimentoautomacao.pages.NovoAtendimentoPage;
import com.thoughtworks.gauge.Step;

import static org.assertj.core.api.Assertions.assertThat;

public class GerarGuiaUrgenciaEEmergencia {
    NovoAtendimentoPage novoAtendimentoPage = new NovoAtendimentoPage();

    @Step("Clicar em Finalizar e Gerar a Guia Urgência e Emergência")
    public void clicarEmGerarEFinalizarGuiaUrgenciaEEmergencia() {
        novoAtendimentoPage.clicarEmFinalizarEGerarGuiaUrgenciaEEmergencia();
    }

    @Step("Verificar se guia de Urgência e Emergência foi gerada com sucesso")
    public void verificarGuiaUrgenciaEEmergenciaGeradaComSucesso() {
        //assertThat(novoAtendimentoPage.retornaResultadoGerarGuiaUrgenciaEEmergencia()).isEqualTo("Consulta de urgência e emergência");
        assertThat(novoAtendimentoPage.retornaResultadoGuiaAutorizada()).isEqualTo("Guia Autorizada");
    }

}
