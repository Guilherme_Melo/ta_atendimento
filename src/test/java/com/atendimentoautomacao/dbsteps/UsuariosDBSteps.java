package com.atendimentoautomacao.dbsteps;

import com.atendimentoautomacao.utils.DBUtils;
import com.atendimentoautomacao.utils.GeneralUtils;

public class UsuariosDBSteps {

    public static String retornaSenhaUsuarioDB(String usuario){
        String query = GeneralUtils.getFileContent("com/atendimentoautomacao/queries/retornaSenhaUsuario.sql");

        query.replace("$usuario", usuario);

        return DBUtils.getQueryResult(query).get(0);
    }
}
