package com.atendimentoautomacao.utils;

import com.atendimentoautomacao.GlobalParameters;
import org.openqa.selenium.WebDriver;

public class DriverUtils {

    public static WebDriver INSTANCE = null;

    public static void createInstance(){
        String browser = GlobalParameters.BROWSER_DEFAULT;
        String execution = GlobalParameters.EXECUTION;

        if (INSTANCE==null){
            if(execution.equals("local")){
                if(browser.equals("chrome")){
                    INSTANCE = BrowserUtils.getLocalChrome();
                }else if(browser.equals("chromeHeadless")){
                    INSTANCE = BrowserUtils.getLocalChromeHeadless();
                }else if(browser.equals("firefox")){
                    INSTANCE = BrowserUtils.getLocalFirefox();
                }else if (browser.equals("ie")){
                    INSTANCE = BrowserUtils.getLocalInternetExplorer();
                }else{
                    try{
                        throw new Exception("O browser informado não existe ou não é suportado pela automação");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            if(execution.equals("remota")){
                if(browser.equals("chrome")){
                    INSTANCE = BrowserUtils.getRemoteChrome();
                }else if(browser.equals("chromeHeadless")){
                    INSTANCE = BrowserUtils.getRemoteChromeHeadless();
                }else if(browser.equals("firefox")){
                    INSTANCE = BrowserUtils.getRemoteFirefox();
                }else if (browser.equals("ie")){
                    INSTANCE = BrowserUtils.getRemoteInternetExplorer();
                }else{
                    try{
                        throw new Exception("O browser informado não existe ou não é suportado pela automação");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static void quitInstace(){
        INSTANCE.quit();
        INSTANCE = null;
    }
}
