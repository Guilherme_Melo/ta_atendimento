package com.atendimentoautomacao.pages;

import com.atendimentoautomacao.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.io.File;

public class NovoAtendimentoPage extends PageBase {
    //Mapping AcoesComuns
    By solicitacaoAutorizacaoButton = By.id("form-selecao:btn-proc-solic-aut");
    By proximoPassoButton = By.id("form-principal:criar-atendimento-button");
    By fecharModalAlertaButton = By.xpath("//a[text()='Fechar']");
    By solicitarAutorizacaoLinkButton = By.id("form-principal:processarLink");
    By finalizarEGerarGuiaButton = By.id("form-status-guia:j_idt2808");
    By getFinalizarEGerarGuiaProrrogacaoButton = By.id("form-status-guia:j_idt5670");
    By anexarDocumentosButton = By.id("anexarDocumentos");
    By adicionarDocumentosButton = By.xpath("//input[@type='file']");
    By incluirAnexoButton = By.id("j_idt2439:btn-anexar-documento");
    By incluirAnexoProrrogacaoButton = By.id("j_idt5301:btn-anexar-documento");
    By tipoAtendimentoComboBox = By.id("form-principal:select-tipo-atendimento");


    private By comboBoxOption(String option) {
        return By.xpath("//*[contains(text(),'" + option + "')]");
    }

    By numeroCarteiraField = By.id("form-principal:numeroCarteira:value");
    By nomeProfissionalSolicitanteField = By.id("form-principal:nomeProfissionalSolicitante:value");
    By nomeHospitalField = By.id("form-principal:razaoLocalSolicitado:value");
    By dataSugeridaField = By.id("form-principal:dtAutorizacao:value");
    By telefoneAutorizacaoGuiaField = By.id("form-status-guia:edit-telefone");
    By emailAutorizacaoGuiaField = By.id("form-status-guia:edit-email");
    By confirmacaoGerarGuiaEmAnaliseField = By.xpath("//h1[text()='Em análise']");
    By confirmacaoGerarGuiaAutorizadaField = By.xpath("//h1[text()='Guia Autorizada']");
    By confirmacaoNumeroCarteiraField = By.xpath("//span[contains(text(),'02965382569000')]");

    //Mapping GerarGuiaAtendimentoDomiciliar
    By tipoIndicacaoAcidenteComboBox = By.id("form-principal:indicacaoAcidente");
    By tipoCaraterAtendimentoComboBox = By.id("form-principal:caraterAtendimento");
    By tipoInternacaoComboBox = By.id("form-principal:tipoInternacao");
    By tipoRegimeInternacaoComboBox = By.id("form-principal:regimeInternacao");
    By contratadoSolicitanteComboBox = By.id("form-principal:idPrestSolicitanteSelect");
    By tipoTabelaProcedimentosComboBox = By.xpath("//select[@id='form-principal:procedimentos-solicitados-list:tabelaProcedimentos:0:procedimento:tabela']");
    By tipoProcedimentoDescricaoComboBox = By.xpath("//input[@id='form-principal:procedimentos-solicitados-list:tabelaProcedimentos:0:procedimento:descricao']");
    By qtdeDiariasSolicitadasField = By.id("form-principal:qtdDiariasSolic");
    By indicacaoClinicaField = By.id("form-principal:indicacaoClinica");
    By cidPrincipalField = By.id("form-principal:cid-principal:value");
    By qtdeSolicitadaTabelaField = By.id("form-principal:procedimentos-solicitados-list:tabelaProcedimentos:0:quantidadeSolicitada");


    //Mapping CapturaGuiaAtendimentoDomiciliar
    By capturarExecucaoButton = By.id("form-selecao:btn-proc-captura-execucao");
    By justificarNaoValidacaoBiometriaButton = By.id("form-principal:bio-ignorar");
    By salvarMotivoJustificativaBiometriaButton = By.id("form-biometria-just:j_idt374");
    By capturarGuiaButton = By.id("table-form:j_idt274:0:btn-capturar-");
    By motivoJustificativaBiometriaField = By.id("form-biometria-just:motivo-justificativa");
    By justificativaTextBriometriaField = By.id("form-biometria-just:justificativa");
    By confirmacaoCapturaGuia = By.xpath("//h1[text()='Guia Capturada']");
    By confirmaBiometriaJustificada = By.xpath("//a[text()=' Biometria Justificada']");

    //Mapping ProrrogacaoCapturaGuiaAtendimentoDomiciliar
    By prorrogarGuiaAtendimentoButton = By.xpath("//div[@data-original-title='Prorrogar a guia de internação']");
    By qtdeDiariasAdicionaisSolicitadasField = By.xpath("//input[@type='text'][@name='form-principal:j_idt3881']");
    By tipoAcomodacaoSolicitadaField = By.xpath("//select[@name='form-principal:j_idt3885']");
    By confirmaGuiaProrrogacaoMenuField = By.xpath("//h1[@style='padding-right: 0']");

    //Mapping GerarGuiaConsultaUrgenciaEEmergencia
    By finalizarEGerarGuiaUrgenciaEEmergenciaButton = By.xpath("//a[@class='btn btn-primary pull-right']");
    By guiaAutorizadaUrgenciaField = By.xpath("//h1[@style='color: #3fac64;float: left;margin-left: 10px;']");

    //Mapping CapturaGuiaConsultaUrgenciaEEmergencia
    By confirmaCapturaGuiaUrgenciaEEmergencia = By.xpath("//span[text()='Guia de Consulta de urgência e emergência']");



    //Actions AcoesComuns
    public void clicarEmSolicitacaoAutorizacao() {
        click(solicitacaoAutorizacaoButton);
    }

    public void selecionarTipoAtendimento(String tipoAtendimento) {
        comboBoxSelectByVisibleText(tipoAtendimentoComboBox, tipoAtendimento);
    }

    public void preencherNumeroCarteira(String numeroCarteira) {
        sendKeys(numeroCarteiraField, numeroCarteira);
        clicarNaOpcaoComboBox(numeroCarteira);
    }

    public void clicarNaOpcaoComboBox(String option) {
        click(comboBoxOption(option));
    }

    public void clicarEmProximoPasso() {
        click(proximoPassoButton);
    }

    public void clicarEmFecharModalAlerta() {
        mouseOver(fecharModalAlertaButton);
        click(fecharModalAlertaButton);
    }

    public void prencherNomeProfissionalSolicitante(String nomeProfissional) {
        sendKeys(nomeProfissionalSolicitanteField, nomeProfissional);
        clicarNaOpcaoComboBox(nomeProfissional);
    }

    public void preencherIdHospital(String idHospital) {
        sendKeys(nomeHospitalField, idHospital);
        clicarNaOpcaoComboBox(idHospital);
    }

    public void preencherTipoTabelaProcedimentosComboBox(String tipoTabelaProcedimentos) {
        comboBoxSelectByVisibleText(tipoTabelaProcedimentosComboBox, tipoTabelaProcedimentos);
    }

    public void preencherTipoProcedimentoTabela(String tipoProcedimento) {
        sendKeys(tipoProcedimentoDescricaoComboBox, tipoProcedimento);
        clicarNaOpcaoComboBox(tipoProcedimento);
    }

    public void preencherQtdeSolicitadaTabela(String qtdeSolicitadaTabela) {
        sendKeys(qtdeSolicitadaTabelaField, qtdeSolicitadaTabela);
    }

    public void clicarEmSolicitarAutorizacaoLink() {
        click(solicitarAutorizacaoLinkButton);
    }

    public void clicarEmFinalizarEGerarGuia() {
        click(finalizarEGerarGuiaButton);
    }

    public void clicarEmAnexarDocumentos(String arquivo) {
        click(anexarDocumentosButton);
        sendKeysWithoutWaitVisible(adicionarDocumentosButton, new File(arquivo).getAbsolutePath());
        click(incluirAnexoButton);
    }

    public void clicarEmAnexarDocumentosProrrogacao(String arquivo) {
        click(anexarDocumentosButton);
        sendKeysWithoutWaitVisible(adicionarDocumentosButton, new File(arquivo).getAbsolutePath());
        ClickJavaScript(incluirAnexoProrrogacaoButton);
    }

    public void preencherTelefoneGuiaDomiciliar(String telefoneGuia) {
        clearAndSendKeys(telefoneAutorizacaoGuiaField, telefoneGuia);
    }

    public void preencherEmailGuiaDomiciliar(String emailGuia) {
        clearAndSendKeys(emailAutorizacaoGuiaField, emailGuia);

    }

    public String retornaResultadoGuiaEmAnalise() {
        click(confirmacaoGerarGuiaEmAnaliseField);
        return getText(confirmacaoGerarGuiaEmAnaliseField);
    }

    public String retornaResultadoGuiaAutorizada() {
        click(confirmacaoGerarGuiaAutorizadaField);
        return getText(confirmacaoGerarGuiaAutorizadaField);
    }

    //Actions CriarGuiaAtendimentoDomiciliar
    public void preencherDataSugerida(String dataSugerida) {
        mouseOver(dataSugeridaField);
        sendKeys(dataSugeridaField, dataSugerida);
        sendKeys(dataSugeridaField, Keys.ENTER);
    }

    public void preencherIndicacaoAcidente(String indicacaoAcidente) {
        mouseOver(tipoIndicacaoAcidenteComboBox);
        comboBoxSelectByVisibleText(tipoIndicacaoAcidenteComboBox, indicacaoAcidente);
    }

    public void preencherCaraterAtendimento(String caraterAtendimento) {
        comboBoxSelectByVisibleText(tipoCaraterAtendimentoComboBox, caraterAtendimento);
    }

    public void preencherTipoInternacao(String tipoInternacao) {
        comboBoxSelectByVisibleText(tipoInternacaoComboBox, tipoInternacao);
    }

    public void preencherRegimeInternacao(String regimeInternacao) {
        comboBoxSelectByVisibleText(tipoRegimeInternacaoComboBox, regimeInternacao);
    }

    public void preencherQtdeDiariasSolicitadas(String qtdeDiariasSolicitas) {
        mouseOver(qtdeDiariasSolicitadasField);
        sendKeys(qtdeDiariasSolicitadasField, qtdeDiariasSolicitas);
    }

    public void preencherIndicacaoClinica(String indicacaoClinica) {
        mouseOver(indicacaoClinicaField);
        sendKeys(indicacaoClinicaField, indicacaoClinica);
    }

    public void prencherCidPrincipal(String cidPrincipal) {
        mouseOver(cidPrincipalField);
        sendKeys(cidPrincipalField, cidPrincipal);
        clicarNaOpcaoComboBox(cidPrincipal);
    }

    public void preencherContratadoSolicitante(String contratadoSolicitante) {
        sendKeys(contratadoSolicitanteComboBox, contratadoSolicitante);
        clicarNaOpcaoComboBox(contratadoSolicitante);
        sendKeys(contratadoSolicitanteComboBox, Keys.ESCAPE);
    }


    //Actions CaputaGuiaAtendimentoDomiciliar
    public void clicarEmCapturarExecucao() {
        click(capturarExecucaoButton);
    }

    public void clicarEmJustificarNaoValidacaoBiometria() {
        click(justificarNaoValidacaoBiometriaButton);
    }

    public void clicarEmSalvarMotivoJustificativaBiometria() {
        click(salvarMotivoJustificativaBiometriaButton);
    }

    public void clicarEmCapturarGuia() {
        click(capturarGuiaButton);
    }

    public void clicarEmProximoPassoBiometria() {
        click(confirmaBiometriaJustificada);
        ClickJavaScript(proximoPassoButton);
    }

    public void clicarEmFecharModalAlertaBiometria() {
        ClickJavaScript(fecharModalAlertaButton);
    }

    public void preencherTipoMotivoJustificativaBiometria(String motivoJustificativaBiometria) {
        click(motivoJustificativaBiometriaField);
        comboBoxSelectByVisibleText(motivoJustificativaBiometriaField, motivoJustificativaBiometria);
    }

    public void preencherJustificativaNãoValidacaoBiometria(String justificativaNaoValidacaoBiometria) {
        ClickJavaScript(motivoJustificativaBiometriaField);
        click(justificativaTextBriometriaField);
        sendKeys(justificativaTextBriometriaField, justificativaNaoValidacaoBiometria);
    }

    public String retornaResultadoCapturaGuia() {
        click(confirmacaoCapturaGuia);
        return getText(confirmacaoCapturaGuia);
    }

    //Actions ProrrogacaoCapturaGuiaAtendimentoDomiciliar
    public void clicarEmProrrogarGuiaAtendimento() {
        click(prorrogarGuiaAtendimentoButton);
    }

    public void preencherQtdeDiariasAdicionaisSolicitadas(String qtdeDiariasAdicionais) {
        click(qtdeDiariasAdicionaisSolicitadasField);
        sendKeys(qtdeDiariasAdicionaisSolicitadasField, qtdeDiariasAdicionais);
    }

    public void preencherTipoAcomodacaoSolicitada(String tipoAcomodacaoSolicitada) {
        click(tipoAcomodacaoSolicitadaField);
        comboBoxSelectByVisibleText(tipoAcomodacaoSolicitadaField, tipoAcomodacaoSolicitada);
    }

    public String retornaResultadoProrrogacaoGuiaDomiciliar() {
        click(confirmaGuiaProrrogacaoMenuField);
        return getText(confirmaGuiaProrrogacaoMenuField);
    }

    public void clicarmEmFinalizarEGerarGuiaProrrogacao() {
        click(getFinalizarEGerarGuiaProrrogacaoButton);
    }

    public void clicarEmFinalizarEGerarGuiaUrgenciaEEmergencia() {
        click(guiaAutorizadaUrgenciaField);
        click(finalizarEGerarGuiaUrgenciaEEmergenciaButton);
    }

    //Actions CapturaGuiaConsultaUrgenciaEEmergencia
    public String retornaCapturaGuiaUrgenciaEEmergencia() {
        click(confirmaCapturaGuiaUrgenciaEEmergencia);
        return getText(confirmaCapturaGuiaUrgenciaEEmergencia);
    }

    //Actions GerarGuiaInternacao
    public String retornaConfirmacaoNumCarteira() {
        click(confirmacaoNumeroCarteiraField);
        return getText(confirmacaoNumeroCarteiraField);
    }
}