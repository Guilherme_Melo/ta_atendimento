package com.atendimentoautomacao.pages;

import com.atendimentoautomacao.bases.PageBase;
import org.openqa.selenium.By;

public class AtendimentoMenuPrincipalPage extends PageBase {
    //Mapping
    By menuTitle = By.xpath("//h3[contains(@class, 'menu-title')]");
    By novoAtendimentoMenu = By.xpath("//a[@href='/atendimentos/novo']/span");


    //Actions
    public String retornaTituloMenuPrincipal(){
        return getText(menuTitle);
    }

    public void clicarEmNovoAtendimento(){
        click(novoAtendimentoMenu);
    }

}
