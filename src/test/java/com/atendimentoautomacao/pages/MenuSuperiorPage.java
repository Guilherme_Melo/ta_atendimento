package com.atendimentoautomacao.pages;

import com.atendimentoautomacao.bases.PageBase;
import org.openqa.selenium.By;

public class MenuSuperiorPage extends PageBase {
    //Mapping
    By usuarioMenuDropDown = By.xpath("//span[@class='mega-name']");

    //Actions
    public String retornaNomeMenuUsuario(){
        return getText(usuarioMenuDropDown);
    }
}
