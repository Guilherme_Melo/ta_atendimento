package com.atendimentoautomacao.pages;

import com.atendimentoautomacao.bases.PageBase;
import org.openqa.selenium.By;

public class SistemasPage extends PageBase {
    //Mapping
    By atendimentoButton = By.xpath("//a[contains(@href, 'atendimento')]");

    //Actions
    public void clicarEmAtendimento(){
        mouseOver(atendimentoButton);
        click(atendimentoButton);
    }
}
