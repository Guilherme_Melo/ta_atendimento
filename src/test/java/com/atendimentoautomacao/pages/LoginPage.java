package com.atendimentoautomacao.pages;

import com.atendimentoautomacao.bases.PageBase;
import org.openqa.selenium.By;

public class LoginPage extends PageBase {
    //Mapping
    By usuarioField = By.id("username");
    By senhaField = By.id("senha");
    By entrarButton = By.id("submit");

    //Actions
    public void preencherUsuario(String usuario){
        sendKeys(usuarioField, usuario);
    }

    public void preencherSenha(String senha){
        sendKeys(senhaField, senha);
    }

    public void clicarEmEntrar(){
        click(entrarButton);
    }
}
