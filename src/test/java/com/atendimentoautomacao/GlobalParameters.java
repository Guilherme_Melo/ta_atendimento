package com.atendimentoautomacao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GlobalParameters {
    public static String BROWSER_DEFAULT;
    public static String EXECUTION;
    public static int TIMEOUT_DEFAULT;
    public static String SELENIUM_HUB;
    public static String URL_DEFAULT;
    public static String USUARIO_DEFAULT;
    public static String SENHA_DEFAULT;
    public static String DOWNLOAD_DEFAULT_PATH;
    public static String DB_URL;
    public static String DB_SID;
    public static String DB_USER;
    public static String DB_PASSWORD;
    public static String NUM_CARTEIRA_DEFAULT;
    public static String TIPO_ATENDIMENTO_DEFAULT;
    public static String MOTIVO_JUSTIFICATIVA_VAL_BIOMETRIA;
    public static String TEXTO_JUSTIFICATIVA_VAL_BIOMETRIA;


    private Properties properties;

    public GlobalParameters(){
        properties = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream("globalParameters.properties");
            properties.load(input);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        BROWSER_DEFAULT = properties.getProperty("browser.default");
        EXECUTION = properties.getProperty("execution");
        TIMEOUT_DEFAULT = Integer.parseInt(properties.getProperty("timeout.default"));
        SELENIUM_HUB = properties.getProperty("selenium.hub");
        URL_DEFAULT = properties.getProperty("url.default");
        USUARIO_DEFAULT = properties.getProperty("usuario.default");
        SENHA_DEFAULT = properties.getProperty("senha.default");
        DOWNLOAD_DEFAULT_PATH = properties.getProperty("download.defaul.path");
        DB_URL = properties.getProperty("db.url");
        DB_SID = properties.getProperty("db.sid");
        DB_USER = properties.getProperty("db.user");
        DB_PASSWORD = properties.getProperty("db.password");
        NUM_CARTEIRA_DEFAULT = properties.getProperty("num.carteira.default");
        TIPO_ATENDIMENTO_DEFAULT = properties.getProperty("tipo.atendimento.default");
        MOTIVO_JUSTIFICATIVA_VAL_BIOMETRIA = properties.getProperty("motivo.justificativa.validacao.biometria");
        TEXTO_JUSTIFICATIVA_VAL_BIOMETRIA = properties.getProperty("justificativa.validacao.biometria.text");
    }
}
