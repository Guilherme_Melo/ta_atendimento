Criar Guia Internação
=====================

Criação de Guia de Internação com sucesso
----------------
* Efetuar login no sistema com usuário default
* Clicar no sistema Atendimento
* Clicar em Novo Atendimento
* Clicar em Solicitação de Autorização
* Informo o tipo de atendimento "Atendimento Domiciliar"
* Informo o número da carteira "02965382569000"
* Clicar em Próximo passo
* Clicar em Fechar
* Informo o nome do Hospital "REAL SOCIEDADE PORTUGUESA DE BENEFICENCIA 16 DE SETEMBRO"
* Informo o nome do Profissional "JOSÉ SLAIBI FILHO"
* Informo a Data Sugerida "28/12/2020"
* Informo o contratado solicitante "714377 - HOSPITAL DE ILHEUS LTDA"
* Informo o tipo de Indicação de Acidente "Não Acidentes"
* Informo o tipo de Carater do atendimento "Eletiva"
* Informo o tipo de Internação "Clínica"
* Informo o tipo de Regime de Internacao "Domiciliar"
* Informo a quantidade de diárias Solicitadas "1"
* Informo a indicação clínica "Teste"
* Informo o CID Principal "E100"
* Informo a Tabela de Procedimentos "19 - TUSS _ Materiais"
* Informo a Descrição do Procedimento "BOLSA COLETORA PARA TRANSPLANTE"
* Informo a Quantidade Solicitada "1"
* Anexo um documento do tipo PDF
* Clicar em Solicitar Autorização
* Informo o número de Telefone "11111112222"
* Informo o email "teste1234@hotmail.com"
* Clicar em Finalizar e Gerar a Guia
* Verificar se guia de Internação foi gerada com sucesso