Prorrogar Guia de Atendimento Docimiliar
=====================

Prorrogação de Guia de Atendimento Domiciliar Capturada com Sucesso
----------------
* Efetuar login no sistema com usuário default
* Acessar guia de Atendimento Domiciliar Capturada
* Clicar em Prorrogar Guia
* Informo o nome do Profissional "JOSÉ SLAIBI FILHO"
* Informo a quantidade de diárias adicionais solicitadas "1"
* Informo o tipo de acomodação "Quarto privativo / Particular"
* Informo a indicação clínica "Teste"
* Informo a Tabela de Procedimentos "19 - TUSS _ Materiais"
* Informo a Descrição do Procedimento "BOLSA COLETORA PARA TRANSPLANTE"
* Anexo um documento para Prorrogação do tipo PDF
* Clicar em Solicitar Autorização
* Informo o número de Telefone "11111112222"
* Informo o email "teste1234@hotmail.com"
* Clicar em Finalizar e Gerar a Guia Prorrogação
* Verificar se Prorrogação foi realizada com sucesso