Gerar Guia de Consulta de Urgência e Emergência
=====================

Criação de Guia de Consulta de Urgência e Emergência com Sucesso
----------------
* Efetuar login no sistema com usuário default
* Clicar no sistema Atendimento
* Clicar em Novo Atendimento
* Clicar em Solicitação de Autorização
* Informo o tipo de atendimento "Consulta de urgência e emergência"
* Informo o número da carteira "02965382569000"
* Clicar em Próximo passo
* Clicar em Fechar
* Informo o nome do Profissional "JOSÉ SLAIBI FILHO"
* Informo o contratado solicitante "714377 - HOSPITAL DE ILHEUS LTDA"
* Informo o tipo de Carater do atendimento "Urgência/Emergência"
* Informo a indicação clínica "Teste"
//* Informo a Tabela de Procedimentos "19 - TUSS _ Materiais"
* Informo a Descrição do Procedimento "MUCOLIN - 30 mg. bl. x 20 cprs."
* Clicar em Solicitar Autorização
* Clicar em Finalizar e Gerar a Guia Urgência e Emergência
* Verificar se guia de Urgência e Emergência foi gerada com sucesso