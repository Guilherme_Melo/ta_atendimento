Captura Guia Atendimento Domiciliar
=====================

Captura de Guia de Atendimento Domiciliar com sucesso
-----------------------------------

* Efetuar login no sistema com usuário default
* Clicar no sistema Atendimento
* Clicar em Novo Atendimento
* Clicar em Captura / Execução
* Informo o tipo de atendimento "Atendimento Domiciliar"
* Informo o número da carteira "02965382569000"
* Clicar em Justificar a Não validação
* Preencher o motivo da não justificativa "Leitor/Bioconnect Com Problemas."
* Preencher o texto da Justificativa "teste"
* Clicar em Salvar justificativa
* Clicar em Próximo passo Biometria
* Clicar em Fechar Biometria
* Clicar em Capturar Guia
* Verificar guia capturada com sucesso